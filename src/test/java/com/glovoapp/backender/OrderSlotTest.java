package com.glovoapp.backender;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OrderSlotTest {

    private OrderSlot orderSlot;
    private Double slotDistanceKm = 0.5;

    private Courier courier;
    private List<Order> orders = new ArrayList<>();

    private Location placaCatalunya = new Location(41.3870154, 2.17004710);
    private Location francescMacia = new Location(41.3925603, 2.1418532); // 3,3km from Placa Catalunya
    private Location tibidabo = new Location(41.4225, 2.1186111111111); // more than 8km from Placa Catalunya
    private Location urquinaona = new Location(41.38944, 2.17333); // 450m from Placa Catalunya
    private Location portalAngelBcn = new Location(41.38646, 2.17196); // 180m from plaça catalunya

    @BeforeEach
    void setUp() throws Exception {
	orderSlot = new OrderSlot(slotDistanceKm);

	courier = new Courier();
	courier.withLocation(placaCatalunya);

	Order order1 = new Order();
	order1.withDescription("order candidte first slot less than 0.5km");
	order1.withId("order-1");
	order1.withPickup(portalAngelBcn);
	order1.withDelivery(urquinaona);

	Order order2 = new Order();
	order2.withDescription("order candidte second slot less than 2.5km");
	order2.withId("order-2");
	order2.withPickup(portalAngelBcn);
	order2.withDelivery(francescMacia);

	Order order3 = new Order();
	order3.withDescription("order candidte second slot less than 6km");
	order3.withId("order-3");
	order3.withPickup(tibidabo);
	order3.withDelivery(francescMacia);

	Order order4 = new Order();
	order4.withDescription("order candidte second slot less than 2.5km");
	order4.withId("order-4");
	order4.withDelivery(portalAngelBcn);
	order4.withPickup(francescMacia);

	orders.add(order3);
	orders.add(order1);
	orders.add(order2);
	orders.add(order4);

    }

    @Test
    void testSlotOrder() {
	HashMap<Integer, List<OrderMaxDistance>> mapOrderSlot = orderSlot.slot(courier, orders);
	assertEquals("order-1", mapOrderSlot.get(0).get(0).getId());
	assertEquals("order-2", mapOrderSlot.get(4).get(0).getId());
	assertEquals("order-4", mapOrderSlot.get(4).get(1).getId());
	assertEquals("order-3", mapOrderSlot.get(11).get(0).getId());
    }

}
