package com.glovoapp.backender;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class VehicleTypeHidingTest {

    private Double courierDistance;
    private Location francescMacia = new Location(41.3925603, 2.1418532);
    private Location placaCatalunya = new Location(41.3870194, 2.1678584);
    private Location tibidabo = new Location(41.4225, 2.1186111111111);
    private Location urquinaona = new Location(41.38944, 2.17333);

    private Courier courier = new Courier();

    private List<Order> orders = new ArrayList<>();

    private VehicleTypeHiding filter;

    @BeforeEach
    void setUp() throws Exception {
	courierDistance = 5.0;

	filter = new VehicleTypeHiding(courierDistance);

	courier = new Courier();
	courier.withId("courierId");
	courier.withVehicle(Vehicle.MOTORCYCLE); // courier that can see others are futher than 5km
	courier.withLocation(placaCatalunya);// position courier Center Barcelona

	orders = new ArrayList<>();
	Order order = new Order();
	order.withId("order-1");
	order.withPickup(placaCatalunya); // pickup location less than 5km from courier location
	order.withDelivery(francescMacia); // delivery location less than 5km from courier location
	orders.add(order);
	order = new Order();
	order.withId("order-2");
	order.withPickup(francescMacia); // pickup location less than 5km from courier location
	order.withDelivery(urquinaona); // delivery location less than 5km from courier location
	orders.add(order);
	// discarded below others for courier without motorcycle or electric scooter
	order = new Order();
	order.withId("order-3");
	order.withPickup(tibidabo); // pickup location more than 5km from courier location
	order.withDelivery(urquinaona); // delivery location less than 5km from courier location
	orders.add(order);
	order = new Order();
	order.withId("order-4");
	order.withPickup(francescMacia); // pickup location less than 5km from courier location
	order.withDelivery(tibidabo); // delivery location more than 5km from courier location
	orders.add(order);
	order = new Order();
	order.withId("order-5");
	order.withPickup(tibidabo); // pickup location more than 5km from courier location
	order.withDelivery(tibidabo); // delivery location more than 5km from courier location
	orders.add(order);

    }

    @Test
    void testMotorcycle() {
	List<Order> candidateOrders = filter.applyFilter(orders, courier);
	assertEquals(orders.size(), candidateOrders.size());
    }

    @Test
    void testBicycle() {
	courier.withVehicle(Vehicle.BICYCLE);
	List<Order> candidateOrders = filter.applyFilter(orders, courier);
	assertTrue(orders.size() >= candidateOrders.size());
	assertTrue(candidateOrders.get(0).getId().equals("order-1"));
	assertTrue(candidateOrders.get(1).getId().equals("order-2"));
	assertFalse(candidateOrders.stream().anyMatch(o -> o.getId().equals("order-5")));
    }

}
