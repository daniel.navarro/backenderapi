package com.glovoapp.backender;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class OrdersByCourierServiceTest {

    @Mock
    OrderRepository orderRepository;
    @Mock
    GlovoBoxHiding glovoBoxFilter;
    @Mock
    VehicleTypeHiding vehicleTypeFilter;
    @Mock
    PrioritiseFilter prioritiseFilter;
    @Mock
    OrderSlot orderSlot;

    private OrdersByCourierService service;

    private List<Order> orders = new ArrayList<>();
    private static String orderKeyWord = "order-keyWord";
    private static String orderWithoutKeyWord = "order-notKeyWord";

    private Courier courier = new Courier();

    @BeforeEach
    void setUp() throws Exception {
	MockitoAnnotations.initMocks(this);

	service = new OrdersByCourierService(orderRepository, glovoBoxFilter, vehicleTypeFilter, prioritiseFilter,
		orderSlot);

	courier.withId("courier-id");

	Order orderWithoutKeyWordBox = new Order();
	orderWithoutKeyWordBox.withId(orderWithoutKeyWord);
	orderWithoutKeyWordBox.withDescription("description without key required glovo box");
	orders.add(orderWithoutKeyWordBox);
	Order orderWithKeyWordBox = new Order();
	orderWithKeyWordBox.withId(orderKeyWord);
	orderWithKeyWordBox.withDescription("description containts keyGlovoWord.");
	orders.add(orderWithKeyWordBox);

	ArrayList<OrderVM> orderVMs = new ArrayList<>();
	orderVMs.add(new OrderVM(orderWithoutKeyWord, "description without key required glovo box"));
	orderVMs.add(new OrderVM(orderKeyWord, "description containts keyGlovoWord."));

	when(orderRepository.findAll()).thenReturn(orders);
	when(glovoBoxFilter.applyFilter(orders, courier)).thenReturn(orders);
	when(vehicleTypeFilter.applyFilter(orders, courier)).thenReturn(orders);
	when(prioritiseFilter.applyPriority(any())).thenReturn(orderVMs);
    }

    @Test
    void testWork() {
	courier.withBox(true);
	List<OrderVM> orderVMs = service.search(courier);
	assertFalse(orderVMs.isEmpty());
    }

    @Test
    void testNotOrdersToManage() {
	when(orderRepository.findAll()).thenReturn(new ArrayList<>());
	List<OrderVM> orderVMs = service.search(courier);
	assertTrue(orderVMs.isEmpty());
    }

}
