package com.glovoapp.backender;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class OrdersByCourierControllerTest {

    @Mock
    CourierRepository courierRepository;
    @Mock
    OrdersByCourierService orderByCourier;

    private OrdersByCourierController controller;

    private Courier courier;
    private OrderVM orderVM;
    private List<OrderVM> orderVMs = new ArrayList<>();

    private String courierId = "courierID";
    private String orderVMId = "orderVM_ID";

    @BeforeEach
    void setUp() throws Exception {
	MockitoAnnotations.initMocks(this);

	controller = new OrdersByCourierController(courierRepository, orderByCourier);

	courier = new Courier();
	courier.withId(courierId);
	orderVM = new OrderVM(orderVMId, "description");
	orderVMs.add(orderVM);
	when(courierRepository.findById(courierId)).thenReturn(courier);
	when(orderByCourier.search(any())).thenReturn(orderVMs);
    }

    @Test
    void testCourierWithOrders() {
	List<OrderVM> orderVMs = controller.orders(courierId);
	verify(courierRepository, times(1)).findById(any());
	verify(orderByCourier, times(1)).search(courier);
	assertEquals(orderVMId, orderVMs.stream().findFirst().get().getId());
    }

    @Test
    void testCourierWithoutOrders() {
	when(orderByCourier.search(courier)).thenReturn(new ArrayList<>());
	List<OrderVM> orderVMs = controller.orders(courierId);
	verify(courierRepository, times(1)).findById(courierId);
	verify(orderByCourier, times(1)).search(courier);
	assertTrue(orderVMs.isEmpty());
    }

    @Test
    void testCourierIsMissing() {
	when(courierRepository.findById(courierId)).thenReturn(null);
	List<OrderVM> orderVMs = controller.orders(courierId);
	Mockito.verifyZeroInteractions(orderByCourier);
	assertTrue(orderVMs.isEmpty());
    }

}
