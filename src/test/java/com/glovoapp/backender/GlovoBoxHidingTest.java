package com.glovoapp.backender;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GlovoBoxHidingTest {

    private List<String> keyWordBoxSet = Arrays.asList("KeyGlovoWord", "OtherWords");
    private Courier courier = new Courier();
    private List<Order> orders = new ArrayList<>();
    private static String orderKeyWord = "order-keyWord";
    private static String orderWithoutKeyWord = "order-notKeyWord";

    private GlovoBoxHiding filter;

    @BeforeEach
    void setUp() throws Exception {
	filter = new GlovoBoxHiding(keyWordBoxSet);

	courier.withId("courier-id");

	Order orderWithoutKeyWordBox = new Order();
	orderWithoutKeyWordBox.withId(orderWithoutKeyWord);
	orderWithoutKeyWordBox.withDescription("description without key required glovo box");
	orders.add(orderWithoutKeyWordBox);
	Order orderWithKeyWordBox = new Order();
	orderWithKeyWordBox.withId(orderKeyWord);
	orderWithKeyWordBox.withDescription("description containts keyGlovoWord.");
	orders.add(orderWithKeyWordBox);
    }

    @Test
    void testCourierWithGlovoBox() {
	courier.withBox(true);
	List<Order> orderByFilter = filter.applyFilter(orders, courier);
	assertFalse(orderByFilter.isEmpty());
	assertTrue(orderByFilter.stream().filter(order -> order.getId().equals(orderKeyWord)).findFirst().isPresent());
	assertTrue(orderByFilter.stream().filter(order -> order.getId().equals(orderWithoutKeyWord)).findFirst()
		.isPresent());
    }

    @Test
    void testWithoutKeyWorks() {
	courier.withBox(false);
	List<Order> orderByFilter = filter.applyFilter(orders, courier);
	assertFalse(orderByFilter.isEmpty());
	assertFalse(orderByFilter.stream().filter(order -> order.getId().equals(orderKeyWord)).findFirst().isPresent());
	assertTrue(orderByFilter.stream().filter(order -> order.getId().equals(orderWithoutKeyWord)).findFirst()
		.isPresent());
    }

}
