package com.glovoapp.backender;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PrioritiseFilterTest {

    private List<String> criteries = new ArrayList<>();
    HashMap<Integer, List<OrderMaxDistance>> slottedOrdersMap = new HashMap<>();

    private PrioritiseFilter prioritiseFilter;

    @BeforeEach
    void setUp() throws Exception {
	criteries.add("vip");
	criteries.add("food");
	prioritiseFilter = new PrioritiseFilter(criteries);

	OrderMaxDistance orderMaxDis1 = new OrderMaxDistance();
	orderMaxDis1.withId("orderMaxDis1");
	orderMaxDis1.withMaxDistance(0.3);
	orderMaxDis1.withFood(true);
	orderMaxDis1.withVip(true);

	OrderMaxDistance orderMaxDis2 = new OrderMaxDistance();
	orderMaxDis2.withId("orderMaxDis2");
	orderMaxDis2.withMaxDistance(0.1);
	orderMaxDis2.withFood(true);
	orderMaxDis2.withVip(false);

	OrderMaxDistance orderMaxDis3 = new OrderMaxDistance();
	orderMaxDis3.withId("orderMaxDis3");
	orderMaxDis3.withMaxDistance(0.2);
	orderMaxDis3.withFood(false);
	orderMaxDis3.withVip(true);

	OrderMaxDistance orderMaxDis4 = new OrderMaxDistance();
	orderMaxDis4.withId("orderMaxDis4");
	orderMaxDis4.withMaxDistance(0.4);
	orderMaxDis4.withFood(false);
	orderMaxDis4.withVip(true);

	OrderMaxDistance orderMaxDis5 = new OrderMaxDistance();
	orderMaxDis5.withId("orderMaxDis5");
	orderMaxDis5.withMaxDistance(0.25);
	orderMaxDis5.withFood(false);
	orderMaxDis5.withVip(false);

	ArrayList<OrderMaxDistance> ordersMaxDistance = new ArrayList<>();
	ordersMaxDistance.add(orderMaxDis1);
	ordersMaxDistance.add(orderMaxDis2);
	ordersMaxDistance.add(orderMaxDis3);
	ordersMaxDistance.add(orderMaxDis4);
	ordersMaxDistance.add(orderMaxDis5);

	slottedOrdersMap.put(0, ordersMaxDistance);
    }

    @Test
    void testVipFood() {
	List<OrderVM> ordersVM = prioritiseFilter.applyPriority(slottedOrdersMap);
	assertEquals("orderMaxDis3", ordersVM.get(0).getId());
	assertEquals("orderMaxDis1", ordersVM.get(1).getId());
	assertEquals("orderMaxDis4", ordersVM.get(2).getId());
	assertEquals("orderMaxDis2", ordersVM.get(3).getId());
	assertEquals("orderMaxDis5", ordersVM.get(4).getId());
    }

    @Test
    void testWithoutCriteria() {
	criteries = new ArrayList<>();
	prioritiseFilter = new PrioritiseFilter(criteries);
	List<OrderVM> ordersVM = prioritiseFilter.applyPriority(slottedOrdersMap);
	assertEquals("orderMaxDis2", ordersVM.get(0).getId());
	assertEquals("orderMaxDis3", ordersVM.get(1).getId());
	assertEquals("orderMaxDis5", ordersVM.get(2).getId());
	assertEquals("orderMaxDis1", ordersVM.get(3).getId());
	assertEquals("orderMaxDis4", ordersVM.get(4).getId());
    }

}
