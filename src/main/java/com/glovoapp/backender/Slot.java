package com.glovoapp.backender;

import java.util.HashMap;
import java.util.List;

/**
 * @author dani
 *
 * @param <C>
 * @param <O>
 * @param <R>
 */
public interface Slot<C, O, R> {

    public HashMap<Integer, List<R>> slot(C courier, List<O> orders);
}
