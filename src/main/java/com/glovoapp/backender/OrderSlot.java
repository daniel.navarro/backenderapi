package com.glovoapp.backender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
class OrderSlot implements Slot<Courier, Order, OrderMaxDistance> {

    private Double slotDistance;

    @Autowired
    public OrderSlot(@Value("${order.slot.distance}") Double slotDistance) {
	super();
	this.slotDistance = slotDistance;
    }

    @Override
    public HashMap<Integer, List<OrderMaxDistance>> slot(Courier courier, List<Order> orders) {
	// I suppose that the distance slot is the maximum distance between courier and
	// pickup or delivery location. The worst case.
	HashMap<Integer, List<OrderMaxDistance>> slotMap = new HashMap<>();
	for (Order order : orders) {
	    Double maxDistance = Math.max(
		    DistanceCalculator.calculateDistance(courier.getLocation(), order.getDelivery()),
		    DistanceCalculator.calculateDistance(courier.getLocation(), order.getPickup()));
	    Integer position = (int) (maxDistance / slotDistance);
	    OrderMaxDistance orderMax = new OrderMaxDistance();
	    orderMax.withOrder(order);
	    orderMax.withMaxDistance(maxDistance);
	    if (slotMap.get(position) == null) {
		List<OrderMaxDistance> listOrder = new ArrayList<OrderMaxDistance>();
		listOrder.add(orderMax);
		slotMap.put(position, listOrder);
	    } else {
		slotMap.get(position).add(orderMax);
	    }
	}
	return slotMap;
    }

}
