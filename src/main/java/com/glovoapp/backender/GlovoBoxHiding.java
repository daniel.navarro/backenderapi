package com.glovoapp.backender;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
class GlovoBoxHiding implements Hidden<Order, Courier> {

    private List<String> keyWordBoxSet;

    @Autowired
    public GlovoBoxHiding(@Value("#{'${courier.box.key_word}'.split(',')}") List<String> keyWordBoxSet) {
	super();
	this.keyWordBoxSet = keyWordBoxSet.stream().filter(s -> !s.isEmpty()).map(String::toUpperCase)
		.collect(Collectors.toList());
    }

    @Override
    public List<Order> applyFilter(List<Order> orders, Courier courier) {
	if (courier.getBox()) {
	    return orders;
	} else {
	    return orders.stream().filter(order -> keyWordBoxSet.stream().parallel()
		    .noneMatch(order.getDescription().toUpperCase()::contains)).collect(Collectors.toList());
	}
    }

}
