package com.glovoapp.backender;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author dani
 *
 */
@Component
public class VehicleTypeHiding implements Hidden<Order, Courier> {

    private Double courierDistance;

    @Autowired
    public VehicleTypeHiding(@Value("${order.courier.distance}") Double courierDistance) {
	super();
	this.courierDistance = courierDistance;
    }

    @Override
    public List<Order> applyFilter(List<Order> orders, Courier courier) {
	if (Vehicle.BICYCLE.equals(courier.getVehicle())) {
	    // to clean the orders with pickup or delivery location are futher than 5km
	    return orders.stream().filter(o -> !isFutherThanDistance(o, courier.getLocation()))
		    .collect(Collectors.toList());
	}
	return orders;
    }

    private Boolean isFutherThanDistance(Order order, Location location) {
	Boolean result = Boolean.FALSE;
	if ((DistanceCalculator.calculateDistance(location, order.getDelivery()) > courierDistance)
		|| (DistanceCalculator.calculateDistance(location, order.getPickup()) > courierDistance)) {
	    result = Boolean.TRUE;
	}
	return result;

    }

}
