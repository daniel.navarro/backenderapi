package com.glovoapp.backender;

import java.util.HashMap;
import java.util.List;

/**
 * @author dani
 *
 * @param <I>
 * @param <O>
 */
public interface Prioritized<I, O> {

    public List<O> applyPriority(HashMap<Integer, List<I>> inputs);
}
