package com.glovoapp.backender;

import java.util.function.Predicate;

/**
 * @author dani
 *
 */
enum OrderCriteria {

    VIP("vip", OrderMaxDistance::getVip), FOOD("food", OrderMaxDistance::getFood);

    private String code;
    private Predicate<OrderMaxDistance> predicate;

    private OrderCriteria(String code, Predicate<OrderMaxDistance> predicate) {
	this.code = code;
	this.predicate = predicate;
    }

    public static Predicate<OrderMaxDistance> getFilter(String code) {
	for (OrderCriteria orderCriteriaEnum : OrderCriteria.values()) {
	    if (orderCriteriaEnum.code.equals(code.toLowerCase())) {
		return orderCriteriaEnum.predicate;
	    }
	}
	return null;
    }

}
