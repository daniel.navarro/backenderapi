package com.glovoapp.backender;

import java.util.List;

/**
 * @author dani
 *
 * @param <I>
 * @param <F>
 */
public interface Hidden<I, F> {

    public List<I> applyFilter(List<I> intputs, F filter);

}
