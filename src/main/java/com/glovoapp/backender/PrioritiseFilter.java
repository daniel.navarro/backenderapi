package com.glovoapp.backender;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author dani
 *
 */
@Component
public class PrioritiseFilter implements Prioritized<OrderMaxDistance, OrderVM> {

    private List<String> orderCriteria;

    @Autowired
    public PrioritiseFilter(@Value("#{'${order.slot.criteria}'.split(',')}") List<String> orderCriteria) {
	super();
	this.orderCriteria = orderCriteria.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());

    }

    @Override
    public List<OrderVM> applyPriority(HashMap<Integer, List<OrderMaxDistance>> slottedOrdersMap) {

	if (orderCriteria.isEmpty()) {
	    // only order by distance
	    for (Map.Entry<Integer, List<OrderMaxDistance>> entry : slottedOrdersMap.entrySet()) {
		slottedOrdersMap.put(entry.getKey(), entry.getValue().stream()
			.sorted(Comparator.comparing(OrderMaxDistance::getMaxDistance)).collect(Collectors.toList()));
	    }
	} else {
	    for (Map.Entry<Integer, List<OrderMaxDistance>> entry : slottedOrdersMap.entrySet()) {
		List<OrderMaxDistance> orderWithCriteria = new ArrayList<>();
		for (String criteria : orderCriteria) {
		    Predicate<OrderMaxDistance> predicateCriteria = OrderCriteria.getFilter(criteria);
		    orderWithCriteria.addAll(entry.getValue().stream().filter(predicateCriteria)
			    .sorted(Comparator.comparing(OrderMaxDistance::getMaxDistance))
			    .collect(Collectors.toList()));
		    entry.getValue().removeIf(predicateCriteria);

		}
		if (entry.getValue() != null && !entry.getValue().isEmpty()) {
		    orderWithCriteria.addAll(
			    entry.getValue().stream().sorted(Comparator.comparing(OrderMaxDistance::getMaxDistance))
				    .collect(Collectors.toList()));
		}
		slottedOrdersMap.put(entry.getKey(), orderWithCriteria);
	    }

	}
	return parserToOrderVM(slottedOrdersMap);
    }

    private List<OrderVM> parserToOrderVM(HashMap<Integer, List<OrderMaxDistance>> slotMap) {
	List<OrderVM> orderVMs = new ArrayList<>();

	for (Map.Entry<Integer, List<OrderMaxDistance>> entry : slotMap.entrySet()) {
	    for (OrderMaxDistance orderMax : entry.getValue()) {
		// System.out.println(orderMax.toString());
		orderVMs.add(new OrderVM(orderMax.getId(), orderMax.getDescription()));
	    }
	}
	return orderVMs;
    }

}
