package com.glovoapp.backender;

/**
 * @author dani
 *
 */
public class OrderMaxDistance extends Order {

    private Double maxDistance;

    public Double getMaxDistance() {
	return maxDistance;
    }

    public void withMaxDistance(Double maxDistance) {
	this.maxDistance = maxDistance;
    }

    public void withOrder(Order order) {
	this.withDelivery(order.getDelivery());
	this.withDescription(order.getDescription());
	this.withFood(order.getFood());
	this.withId(order.getId());
	this.withPickup(order.getPickup());
	this.withVip(order.getVip());
    }

    @Override
    public String toString() {
	return "OrderMaxDistance [maxDistance=" + maxDistance + "] Vip[" + super.getVip() + "] Food[" + super.getFood()
		+ "] Description [" + super.getDescription() + "] id[" + super.getId() + "]";
    }

}
