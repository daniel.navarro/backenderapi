package com.glovoapp.backender;

import java.util.List;

/**
 * @author dani
 *
 * @param <F>
 * @param <R>
 */
public interface Searchable<F, R> {

    public List<R> search(F object);
}
