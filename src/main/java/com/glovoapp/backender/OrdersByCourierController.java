package com.glovoapp.backender;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author dani
 *
 */
@Controller
public class OrdersByCourierController {

    private final CourierRepository courierRepository;

    private final OrdersByCourierService orderByCourier;

    @Autowired
    public OrdersByCourierController(CourierRepository courierRepository, OrdersByCourierService orderByCourier) {
	super();
	this.courierRepository = courierRepository;
	this.orderByCourier = orderByCourier;
    }

    @RequestMapping(value = "/orders/{courierId}", method = RequestMethod.GET)
    @ResponseBody
    List<OrderVM> orders(@PathVariable("courierId") String courierId) {
	List<OrderVM> orderVMList = new ArrayList<>();
	Courier courier = courierRepository.findById(courierId);
	if (courier != null) {
	    return orderByCourier.search(courier);
	}
	return orderVMList;
    }

}
