package com.glovoapp.backender;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author dani
 *
 */
@Component
public class OrdersByCourierService implements Searchable<Courier, OrderVM> {

    private final OrderRepository orderRepository;
    private GlovoBoxHiding glovoBoxFilter;
    private VehicleTypeHiding vehicleTypeFilter;
    private PrioritiseFilter prioritiseFilter;
    private OrderSlot orderSlot;

    @Autowired
    public OrdersByCourierService(OrderRepository orderRepository, GlovoBoxHiding glovoBoxFilter,
	    VehicleTypeHiding vehicleTypeFilter, PrioritiseFilter prioritiseFilter, OrderSlot orderSlot) {
	this.orderRepository = orderRepository;
	this.glovoBoxFilter = glovoBoxFilter;
	this.vehicleTypeFilter = vehicleTypeFilter;
	this.prioritiseFilter = prioritiseFilter;
	this.orderSlot = orderSlot;
    }

    @Override
    public List<OrderVM> search(Courier courier) {
	List<Order> orders = orderRepository.findAll();
	if (orders.isEmpty()) {
	    return new ArrayList<>();
	} else {
	    orders = glovoBoxFilter.applyFilter(orders, courier);
	    orders = vehicleTypeFilter.applyFilter(orders, courier);
	    return prioritiseFilter.applyPriority(orderSlot.slot(courier, orders));
	}
    }

}
